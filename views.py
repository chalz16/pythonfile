from django.http import HttpResponse
from .models import Question
#from django.template import loader
from django.shortcuts import get_object_or_404,render

#print (display)

import json
import requests
import pandas as pd
import datetime

r = requests.get("http://data.rcc-acis.org/StnData?sid=kbtr&sdate=20180101&edate=20181231&elems=maxt,mint,pcpn&output=json")
data =json.loads(r.text)
df =pd.DataFrame(data['data'])
df.columns = ['date','max','min','rain']
month = 0
i,days,first,last,temp = 0,0,0,0,1
fo = open("output.txt","w")
#replace T with 0 wherever present, for sake of finding total precipitation for each month
rep_rain = df['rain'].replace({'T': 0})
mon = datetime.datetime.strptime(df['date'][i], "%Y-%m-%d").strftime("%m")
month = int(mon)
climate = [None] * 12
while i < (len(df) + 1):
    if i == len(df):
       pass
    else:
       mon = datetime.datetime.strptime(df['date'][i], "%Y-%m-%d").strftime("%m")
       temp = int(mon)
    if temp != month or i == len(df):
       last = i
       max_mean = pd.to_numeric(df['max'][first:(last)]).mean() #calculate avg max temp of that month
       min_mean = pd.to_numeric(df['min'][first:(last)]).mean() #calculate avg min temp of that month
       rain_sum = pd.to_numeric(rep_rain[first:(last)]).sum() #calculate total precipitation of that month
       display = str(max_mean) + ', ' + str(min_mean) + ', ' + str(rain_sum) + '\n'
       #output the 3 paramters for each month into the text file
       
       year_month = datetime.datetime.strptime(df['date'][1], "%Y-%m-%d").strftime("%Y") + '-' + str(month)
       climate[temp-1] = pd.DataFrame([[year_month, str(max_mean), str(min_mean), str(rain_sum)]])
       climate[temp-1] = climate[temp-1].to_string(index=False)
       fo.write(display)
       month = month + 1
       first = last               
    i = i + 1
    
print(display)
fo.close()
print(climate)

def index(request):
    return HttpResponse(climate)

def detail(request, question_id):
    question = get_object_or_404(Question, pk=question_id)
    return render(request, 'polls/detail.html', {'question': question})

def results(request, question_id):
    response = "You're looking at the results of question %s."
    return HttpResponse(response % question_id)

def vote(request, question_id):
    return HttpResponse("You're voting on question %s." % question_id)